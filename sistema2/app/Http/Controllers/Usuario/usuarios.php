<?php

namespace App\Http\Controllers\Usuario; //se declara el controlador

use App\Models\User\Usuario;
//use App\Models\User\Sexo;

// se agrega libreria para ejecutar el request
use Illuminate\Http\Request;

use Auth;


use App\Http\Controllers\Controller;

class usuarios extends Controller
{
  public function _construct()
  {
     
  }

    public function getlogin()
    {
       return view('auth.login');
    }

    public function login()
    {
       $cc = $this->validate(request(),['nombre'=>'required|string','password' => 'required|string']);
       //return $cc;
       if(Auth::attempt($cc))
       {
         return redirect('principal');
       }
       else {
         ?>
             <script>
                         alert("Error: Usuario y/o Contraseña incorrectos");
                         document.location.href = "ingreso";
             </script>
        <?php
       }
    }
    //
    public function index()
    {
      return view('Usuario/index');
    }

    public function principal()
    {
      $consulta_ver = Usuario::all();

      return view('Usuario/principal')->with('usuario_t',$consulta_ver);
    }

    public function insertar_pro()
    {
      $consulta = Usuario::select('nombre','tipo','marca','folio','precioFabrica','precioVenta',
                                'caracteristicas','cantidad','fechaIngreso','fechaSalida','activo')->get();
      return view('CRUD/insertar')->with('usuario',$consulta);
    }

    
    public function insertar_producto(Request $request)
    {
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $marca = $request->input('marca');
        $folio = $request->input('folio');
        $precioFabrica = $request->input('precioFabrica');
        $precioVenta = $request->input('precioVenta');
        $caracteristicas = $request->input('caracteristicas');
        $cantidad = $request->input('cantidad');
        $fechaIngreso = $request->input('fechaIngreso');
        $fechaSalida = $request->input('fechaSalida');
        $activo = $request->input('activo');
        
      Usuario::create(['nombre' => $nombre,'tipo' => $tipo,'marca' => $marca, 'folio' => $folio, 'precioFabrica' => $precioFabrica, 'precioVenta' => $precioVenta, 'caracteristicas' => $caracteristicas, 'cantidad' => $cantidad, 'fechaIngreso' => $fechaIngreso, 'fechaSalida' => $fechaSalida, 'activo' => $activo]);

      return redirect()->to('principal');
    }

    public function editar_pro($id)
    {
      $edit_consulta = Usuario::select('id','nombre','tipo','marca','folio','precioFabrica','precioVenta',
                                        'caracteristicas','cantidad','fechaIngreso','fechaSalida','activo')
                       ->where('id','=',$id)
                       ->take(1)
                       ->first();

                       //return $edit_consulta;
      
      return view('CRUD/editar')->with('usuario_u',$edit_consulta);
    }

    public function actualizar_pro(Request $request,$id)
    {
        $usuario = Usuario::find($id);
        $usuario->nombre = $request->usuario;
        $usuario->tipo = $request->tipo;
        $usuario->marca = $request->marca;
        $usuario->folio = $request->folio;
        $usuario->precioFabrica = $request->precioFabrica;
        $usuario->precioVenta = $request->precioVenta;
        $usuario->caracteristicas = $request->caracteristicas;
        $usuario->cantidad = $request->cantidad;
        $usuario->fechaIngreso = $request->fechaIngreso;
        $usuario->fechaSalida = $request->fechaSalida;
        $usuario->activo = $request->activo;
        $usuario->save();


        return redirect('principal');
    }

    public function eliminar_pro($id)
    {
      $edit_consulta = Usuario::select('id','nombre','tipo','marca','folio','precioFabrica','precioVenta',
                                        'caracteristicas','cantidad','fechaIngreso','fechaSalida','activo')
      ->where('id','=',$id)
      ->take(1)
      ->first();

      return view('CRUD/eliminar')->with('usuario',$edit_consulta);
    }

     public function borrar_pro(Request $request,$id)
    {
        $usuario = Usuario::find($id);

        $usuario->activo = $request->activo;
        $usuario->save();


        return redirect('principal');
    }

    public function eliminar_total_u($id)
    {
      $edit_consulta = Usuario::select('id','nombre','tipo')
      ->where('id','=',$id)
      ->take(1)
      ->first();

      return view('CRUD/eliminar_todo_u')->with('usuario',$edit_consulta);
    }

    public function borrar_todo_u(Request $request,$id)
    {
        $usuario = Usuario::find($id);

        $usuario->delete();

        return redirect('principal');
    }

   
}