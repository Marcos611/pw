<?php

namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected  $table = 'inventario';
    // aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    // aqui los elementos a mostrarse de la tabla en cuestion
    protected $fillable = [
      'id','nombre','tipo','marca','folio','precioFabrica','precioVenta','caracteristicas','cantidad','fechaIngreso','fechaSalida','estado'
    ];
}