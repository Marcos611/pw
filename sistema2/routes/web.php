<?php

Route::get('/', function () {
    return view('Usuario\index');
});
// pagina de logueo
Route::get('ingreso','Usuario\usuarios@getlogin');
Route::post('login2','Usuario\usuarios@login');

// pagina donde contendra un menu
Route::get('index','Usuario\usuarios@index');



// pagina principal donde contendra una tabla del crud
Route::get('principal','Usuario\usuarios@principal');

// aqui esta el formulario insertar - vista
Route::get('insertar_pro','Usuario\usuarios@insertar_pro');
// aqui esta el post que es donde va a hacer el registro
Route::post('insertar_producto','Usuario\usuarios@insertar_producto');

//aqui esta el formulario edicion - vista
Route::get('editar_pro/{id}','Usuario\usuarios@editar_pro');
Route::put('actualizar_pro/{id}','Usuario\usuarios@actualizar_pro');

// formulario para eliminar por bandera un registro

Route::get('eliminar_pro/{id}','Usuario\usuarios@eliminar_pro');
Route::put('borrar_pro/{id}','Usuario\usuarios@borrar_pro');

// formulario para eliminar de la base de datos un registro
Route::get('eliminar_total_u/{id}','Usuario\usuarios@eliminar_total_u');
Route::put('borrar_todo_u/{id}','Usuario\usuarios@borrar_todo_u');


Route::get('consultajson/{id}','Usuario\usuarios@cargarjs');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
?>
