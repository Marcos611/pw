@extends('Usuario.index')

@section('title',"Baja del usuario total")

@section('content')


<h1 align="center">ELIMINAR PRODUCTO DEFINITIVAMENTE</h1>

{!!Form::open(array('url'=>'borrar_todo_u/'.$usuario->id,'method'=>'PUT'
,'autocomplete'=>'off','enctype'=>'multipart/form-data'))!!}

 {{ csrf_field() }}

 {{ method_field('PUT') }}

 {!!form::hidden('activo',$usuario->activo,['id'=>'activo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'readonly'=>'true'])!!}

{!!form::label('Usuario')!!}

{!!form::text('usuario20',$usuario->nombre,['id'=>'usuario','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'readonly'=>'true'])!!}

{!!form::label('Tipo del usuario')!!}

{!!form::text('tipo',$usuario->tipo,['id'=>'tipo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'readonly'=>'true'])!!}

{!!form::label('Activo')!!}

   <select name="activo" class="form-control">
             <option value="">Seleccione</option>
             <option value="1">Activo</option>
             <option value="0">Desactivado</option>
   </select>

   <br>
   
{!!form::submit('Eliminar',['name'=>'grabar','id'=>'grabar'
,'content'=>'<span>Eliminar</span>','class'=>'btn btn-warning btn-sm m-t-10'])!!}
{!!Form::close()!!}

@endsection