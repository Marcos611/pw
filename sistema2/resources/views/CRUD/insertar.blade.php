@extends('Usuario.index')

@section('title',"REGISTRO DE PRODUCTO")

@section('content')

<script src="../js/jquery.min.js"></script>
<script src="../js/jquery2.min.js"></script>

<script src="../js/cargar.js"></script>

<h1 align="center">REGISTRO DE PRODUCTO</h1>

{!!Form::open(array('url'=>'insertar_producto','method'=>'POST'
,'autocomplete'=>'off','enctype'=>'multipart/form-data'))!!}

 {{ csrf_field() }}

{!!form::label('Producto')!!}

{!!form::text('nombre',null,['id'=>'nombre','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese nombre producto'])!!}

{!!form::label('Tipo producto')!!}

{!!form::text('tipo',null,['id'=>'tipo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese tipo de producto'])!!}



   {!!form::label('Marca producto')!!}

{!!form::text('marca',null,['id'=>'marca','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese marca de producto'])!!}

<br>
{!!form::label('Folio producto')!!}

{!!form::text('folio',null,['id'=>'folio','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}


   <br>
{!!form::label('Precio de Fábrica producto')!!}

{!!form::text('precioFabrica',null,['id'=>'precioFabrica','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}


<br>
{!!form::label('Precio de venta producto')!!}

{!!form::text('precioVenta',null,['id'=>'precioVenta','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}   


<br>
{!!form::label('Caracteristicas producto')!!}

{!!form::text('caracteristicas',null,['id'=>'caracteristicas','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}


<br>
{!!form::label('Cantidad producto')!!}

{!!form::text('cantidad',null,['id'=>'cantidad','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}


<br>
{!!form::label('Fecha de ingreso producto')!!}

{!!form::text('fechaIngreso',null,['id'=>'fechaIngreso','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}   


<br>
{!!form::label('Fecha de salida producto')!!}

{!!form::text('fechaSalida',null,['id'=>'fechaSalida','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}   


<br>
{!!form::label('Activo')!!}

{!!form::text('activo',null,['id'=>'activo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese folio de producto'])!!}   
                                     
<br>

{!!form::submit('Grabar',['name'=>'grabar','id'=>'grabar'
,'content'=>'<span>Grabar</span>','class'=>'btn btn-warning btn-sm m-t-10'])!!}
{!!Form::close()!!}

@endsection