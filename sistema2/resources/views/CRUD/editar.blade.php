@extends('Usuario.index')

@section('title',"Edicion del usuario")

@section('content')


<h1 align="center">EDITAR PRODUCTO</h1>

{!!Form::open(array('url'=>'actualizar_pro/'.$usuario_u->id,'method'=>'PUT'
,'autocomplete'=>'off','enctype'=>'multipart/form-data'))!!}

 {{ csrf_field() }}

 {{ method_field('PUT') }}

{!!form::label('PRODUCTO')!!}

{!!form::text('usuario',$usuario_u->nombre,['id'=>'usuario','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese nombre producto'])!!}


{!!form::label('TIPO DE PRODUCTO')!!}

{!!form::text('tipo',$usuario_u->tipo,['id'=>'tipo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese el tipo del producto'])!!}


{!!form::label('MARCA DE PRODUCTO')!!}

{!!form::text('marca',$usuario_u->marca,['id'=>'marca','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la marca del producto'])!!}


{!!form::label('FOLIO DE PRODUCTO')!!}

{!!form::text('folio',$usuario_u->folio,['id'=>'folio','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese el folio del producto'])!!}


{!!form::label('PRECIO DE FÁBRICA DE PRODUCTO')!!}

{!!form::text('precioFabrica',$usuario_u->precioFabrica,['id'=>'precioFabrica','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la marca del producto'])!!}


   {!!form::label('PRECIO DE VENTA DE PRODUCTO')!!}

{!!form::text('precioVenta',$usuario_u->precioVenta,['id'=>'precioVenta','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese el precio de venta del producto'])!!}


   {!!form::label('CARACTRÍSTICAS DE PRODUCTO')!!}

{!!form::text('caracteristicas',$usuario_u->caracteristicas,['id'=>'caracteristicas','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la marca del producto'])!!}


   {!!form::label('CANTIDAD DE PRODUCTO')!!}

{!!form::text('cantidad',$usuario_u->cantidad,['id'=>'cantidad','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la cantidad del producto'])!!}


{!!form::label('FECHA DE INGRESO DE PRODUCTO')!!}

{!!form::text('fechaIngreso',$usuario_u->fechaIngreso,['id'=>'fechaIngreso','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la fecha de ingreso del producto'])!!}


   {!!form::label('FECHA DE SALIDA DE PRODUCTO')!!}

{!!form::text('fechaSalida',$usuario_u->fechaSalida,['id'=>'fechaSalida','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la fecha de salida del producto'])!!}


{!!form::label('DISPONIBLE DE PRODUCTO')!!}

{!!form::text('activo',$usuario_u->activo,['id'=>'activo','class'=>'form-control'
   ,'style'=>'text-transform:uppercase'
   ,'onkeyup'=>'javascript:this.value=this.value.toUpperCase()'
   ,'placeholder'=>'Ingrese la marca del producto'])!!}



  

   <br>
   
{!!form::submit('Editar',['name'=>'grabar','id'=>'grabar'
,'content'=>'<span>Editar</span>','class'=>'btn btn-warning btn-sm m-t-10'])!!}
{!!Form::close()!!}

@endsection